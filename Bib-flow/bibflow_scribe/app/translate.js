var parseString = require('xml2js').parseString;
var maxMatchLength = 50, generateMatch;
var proxy=require('./index.js');
var http = require('q-io/http');

proxyHelper = function(qin) {
    var request, a;
    /*
        request = http.request('http://' + conf.host + ((branch !== null) ? branch : '') + conf.path +  '?' + conf.arg + '=' + qin + conf.queryArgs);
    */
/*
    request=http.request('http://experiment.worldcat.org/entity/work/data/'+qin+'.jsonld);
//request=http.request('http://experiment.worldcat.org/entity/work/data/54619668.jsonld');

    return request.then(function(response) {
  // response.setEncoding('utf8');
        var json=response.statusCode;
       console.log('response headers-----'+qin +json);
        return response.body.read().then(function(answer) {
        // console.log('answer service from OCLC number-----' +answer);
            return answer;
        });
    });*/

    request = http.request('http://experiment.worldcat.org/entity/work/data/'+qin+'.jsonld');
    request.then(function(response) {
    	if(response.status == 200){
    	console.log('next response-----' +response);
        return response.body.read().then(function(answer) {
        	console.log('next answer-----' +answer);
            return answer;
        });
    	}
    });
    
//http.get('http://experiment.worldcat.org/entity/work/data/'+qin+'.jsonld', function(res) {
//  console.log("statusCode: ", res.statusCode); // <======= Here's the status code
//  console.log("headers: ", res.headers);
//
//  res.on('data', function(d) {
//   // process.stdout.write(d);
//  });
//
//}).on('error', function(e) {
//  console.error(e);
//});

};



generateMatch = function(uri, label, source,refId) {
    var match = {
        'uri': uri,
        'source': source,
        'fullLabel': label,
        'refId' :refId
    };
    if (label.length > maxMatchLength) {
        match['label'] = label.substr(0, maxMatchLength) + '...';
    } else {
        match['label'] = label;
    }
    return match;
};

var exports = module.exports = {
    agrovoc: function(res) {
        var i, answer, json;
        json = JSON.parse(res);
        answer = [];
        for (i = 0; i < json.results.length; i++) {
            answer.push(generateMatch(
                json.results[i].uri,
                json.results[i].prefLabel,
                'agrovoc'
            ));
        }
        return answer;
    },
    fast: function(res) {
        var i, answer, json, term, id;
        answer = [];
        json = JSON.parse(res);
        for (i = 0; i < json.response.docs.length; i++) {
            if (typeof json.response.docs[i].idroot !== 'undefined') {
                id = json.response.docs[i].idroot.substring(3).replace(/^0+/, '');
                answer.push(generateMatch(
                    'http://experimental.worldcat.org/fast/' + id + '/',
                    json.response.docs[i].suggestall,
                    'fast'
                ));
            }
        }
        return answer;
    },
    lc: function(res) {
        var i, answer, json;
        answer = [];
        json = JSON.parse(res);
        for (i = 0; i < json[1].length; i++) {
            answer.push(generateMatch(
                json[3][i],
                json[1][i],
                'lc'
            ));
        }
        return answer;
    },
    viaf: function(res) {
        var i, answer, json;
        answer = [];
        json = JSON.parse(res);
        for (i = 0; i < json.result.length; i++) {
            answer.push(generateMatch(
                'http://viaf.org/viaf/' + json.result[i].viafid,
                json.result[i].term,
                'viaf'
            ));
        }
        return answer;
    },
    oclc: function(res) {
        var i, answer, json;
        answer = [];
        parseString(res, function (err, result) {
        	 for (i = 0; i < result.feed.entry.length; i++) {
        		 answer.push(generateMatch(
        				    result.feed.entry[i].id,
        				    result.feed.entry[i].title,
        	                'oclc',
        	                result.feed.entry[i]['oclcterms:recordIdentifier']
        	            ));

            /*   console.log( "what is match for Title look Up" +generateMatch(
                            result.feed.entry[i].id,
                            result.feed.entry[i].title,
                            'oclc'
                        ));*/
        	 }
        });
        return answer;
    },
	ISBN : function(res) {

		var i, answer, json;
		answer = [];
		json = JSON.parse(res);
		for (i = 0; i < json.list[0].oclcnum.length; i++) {
			var number = json.list[0].oclcnum[i];
			answer.push(number);
		}
		return answer;
	},
	OCLCmulti : function(res) {

        //    console.log("JSON data from OCLC-multi response"+res);
        var i, answer, json;
        answer = [];
        json = JSON.parse(res);
        return json;
    },
    oclcSingle : function(res) {

        //    console.log("JSON data from OCLC-multi response"+res);
        var i, answer, json;
        answer = [];
        json = JSON.parse(res);
        return json;
    },
};
